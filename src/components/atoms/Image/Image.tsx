import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface ImageProps {
    className: string;
    src: string,
    alt: string,
}

const StyledImage = styled.img<ImageProps>`
  &.sidebar-icon {width: 16px; height: 10px; color: #fff;}
  &.icon-centos {width: 15px; height: 15px;}
`;

const Image = ({
                   className,
                   src,
                   alt
               }: ImageProps) => {
    const classCandidate = [className];
    const commonProps = {};
    return <StyledImage {...commonProps} className={cn(classCandidate)} src={src} alt={alt}>
    </StyledImage>
};

export default memo(Image);