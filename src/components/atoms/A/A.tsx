import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface AProps {
    children?: React.ReactNode;
    className?: string;
    url?: string;

    [prop: string]: any;

    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledA = styled.a<AProps>`
  &.sidebar-title {display: block; padding: 10px; color: #fff; font-size: 25px;}
  &.sidebar-link, a&.sidebar-link {position: relative; display: block; padding: 10px; cursor: pointer; color: #e1e1e1;}
  &.sidebar-link:hover {background: #222e3c;}
  //&.hover-tab:hover {color: #fff; border: 1px solid #43a7b7; background-color: #43a7b7;}
  &.active-tab {color: #fff !important; border: 1px solid #43a7b7; background-color: #43a7b7;}
  &.btn {padding: 5px 9px 5px 9px; color: #fff; border-radius: 5%; background-color: #4fa5db;}
  &.hover-tab:hover > span.tab-number {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    width: 18px;
    height: 18px;
    padding: 3px;
    color: #43a7b7;
    border: 1px solid #fff;
    border-radius: 50%;
    background: #fff;
    font-size: 12px;
  }
`;

const A = ({
                  children,
                  className,
              }: AProps) => {
    const classCandidate = [className];
    const commonProps = {
    };
    return <StyledA {...commonProps} className={cn(classCandidate)}>
        {children}
    </StyledA>
};

export default memo(A);
