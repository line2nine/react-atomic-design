import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface TableProps {
    children?: React.ReactNode;
    flex?: number | 'auto';
    className?: string;

    [prop: string]: any;

    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledTable = styled.table<TableProps>`
  &.table-1 {width: 100%; text-align: left; border: 1px solid #b3b3b3;}
  &.table-1 th {width: 15%; padding: 0 0 0 20px; border-bottom: 1px solid #b3b3b3; background-color: #e5e5e5; font-weight: bold;}
  &.table-1 td {padding: 10px; border-bottom: 1px solid #b3b3b3; background-color: #fff;}

  &.table-2 {width: 100%; border: 1px solid #b3b3b3;}
  &.table-2 th {font-weight: bold;}
  &.table-2, th, td {padding: 10px;}
  &.table-2 th, td {border-bottom: 1px solid #b3b3b3;}
`;

const Table = ({
                     children,
                     flex = 'auto',
                     className,
                 }: TableProps) => {
    const classCandidate = [className];
    const commonProps = {
        flex,
    };
    return <StyledTable {...commonProps} className={cn(classCandidate)}>
        {children}
    </StyledTable>
};

export default memo(Table);
