import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface TDProps {
    children?: React.ReactNode;
    flex?: number | 'auto';
    className?: string;

    [prop: string]: any;

    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledTD = styled.td<TDProps>`
    
`;

const TD = ({
                   children,
                   flex = 'auto',
                   className,
               }: TDProps) => {
    const classCandidate = [className];
    const commonProps = {
        flex,
    };
    return <StyledTD {...commonProps} className={cn(classCandidate)}>
        {children}
    </StyledTD>
};

export default memo(TD);
