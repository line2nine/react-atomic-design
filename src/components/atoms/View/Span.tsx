import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface SpanProps {
    children?: React.ReactNode;
    className?: string;
}

const StyledSpan = styled.span<SpanProps>`
    &.notify-number {
      position: absolute;
      display: inline-flex;
      align-items: center;
      justify-content: center;
      width: 17px;
      height: 11px;
      padding: 0;
      color: #fff;
      border: 1px solid orangered;
      border-radius: 40%;
      background: orangered;
      font-size: 10px;
    }
  &.console {margin-left: 65px; padding: 4px 15px 4px 15px; color: #43a7b7; border: 1px solid #43a7b7; border-radius: 15px;font-size: 12px;}
  &.content-header {font-size: 25px; font-weight: bold;}
  &.bookmark-number {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    width: 15px;
    height: 15px;
    padding: 3px;
    color: #fff;
    border: 1px solid #222e3c;
    border-radius: 50%;
    background: deepskyblue;
    font-size: 12px;
  }
  &.tab-number {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    width: 18px;
    height: 18px;
    padding: 3px;
    color: #fff;
    border: 1px solid #43a7b7;
    border-radius: 50%;
    background: #43a7b7;
    font-size: 12px;
  }
  &.tab-number-active {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    width: 18px;
    height: 18px;
    padding: 3px;
    color: #43a7b7;
    border: 1px solid #fff;
    border-radius: 50%;
    background: #fff;
    font-size: 12px;
  }
`;

const Span = ({
                 children,
                 className,
             }: SpanProps) => {
    const classCandidate = [className];
    const commonProps = {
    };
    return <StyledSpan {...commonProps} className={cn(classCandidate)}>
        {children}
    </StyledSpan>
};

export default memo(Span);
