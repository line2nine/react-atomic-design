import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface DivProps {
    children?: React.ReactNode;
    className?: string;

    [prop: string]: any;

    bgColor?: string;
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledDiv = styled.div<DivProps>`
  &.wrapper {display: flex; align-items: stretch; width: 100%; background: #e9eced;}
  &.sidebar-content {display: flex; flex-direction: column; height: 100vh; position: relative; background: #1c2427;}
  &.sidebar-nav {flex-grow: 1; margin-bottom: 0; padding-left: 0;list-style: none;}
  &.sidebar-bookmark {padding: 10px 0 10px 0; border: 1px solid #595959; border-right: none; border-left: none;}
  &.sidebar-bookmark > a {color: #009be5 !important;}
  &.sidebar-recently-viewed > a {color: #009be5 !important;}
  &.sidebar-menu {background-color: #2e3f52;}
  &.sidebar-group {background-color: #121a26;}
  &.sidebar-head {background-color: #040d18;}
  &.sidebar-item > ul {background-color: #2e3f52 !important;}
  &.header-title {margin-left: 300px;color: #fff;}
  &.header-menu > a > i {color: #fff;}
  &.container {
    display: flex;
    overflow: hidden;
    flex-direction: column;
    width: 100%;
    min-height: 100vh;
    border-top-left-radius: 0;
    border-bottom-left-radius: 0
  }
  &.content {flex: 1; width: 80vw; max-width: 100vw; padding: 80px 80px 0 60px;}
  &.content-tab-menu > a {padding: 5px 20px 5px 20px; color: black;}
  &.content-tab-menu > a:hover {color: #fff; border: 1px solid #43a7b7; background-color: #43a7b7;}
  &.content-description {margin-top: 10px; padding: 20px;}
  &.content-description > h2 {font-size: 14px; font-weight: 520;}
  &.user {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    width: 25px;
    height: 25px;
    padding: 0;
    color: #fff;
    border: 1px solid grey;
    border-radius: 50%;
    background: grey;
    font-size: 12px;
  }
`;

const Div = ({
                  children,
                  className,
                  bgColor
              }: DivProps) => {
    const classCandidate = [className];
    const commonProps = {
        bgColor
    };
    return <StyledDiv {...commonProps} className={cn(classCandidate)}>
            {children}
        </StyledDiv>
};

export default memo(Div);
