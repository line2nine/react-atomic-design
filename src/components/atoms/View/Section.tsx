import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface SectionProps {
    children?: React.ReactNode;
    flex?: number | 'auto';
    className?: string;

    [prop: string]: any;

    bgColor?: string;
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledSection = styled.section<SectionProps>`
    &.notify-number {
      position: absolute;
      display: inline-flex;
      align-items: center;
      justify-content: center;
      width: 17px;
      height: 11px;
      padding: 0;
      color: #fff;
      border: 1px solid orangered;
      border-radius: 40%;
      background: orangered;
      font-size: 10px;
    }
  &.console {margin-left: 65px; padding: 4px 15px 4px 15px; color: #43a7b7; border: 1px solid #43a7b7; border-radius: 15px;font-size: 12px;}
  &.content-header {font-size: 25px; font-weight: bold;}
  &.content-detail > hr {display: block; margin-top: 5px;}
  &.section-content-description-1 {box-sizing: content-box; margin-bottom: 20px; border-top: 2px solid #43a7b7; background-color: #ffffff;}
  &.section-content-description-2 {box-sizing: content-box; background-color: #ffffff;}
`;

const Section = ({
                  children,
                  flex = 'auto',
                  className,
                  bgColor
              }: SectionProps) => {
    const classCandidate = [className];
    const commonProps = {
        flex,
        bgColor
    };
    return <StyledSection {...commonProps} className={cn(classCandidate)}>
        {children}
    </StyledSection>
};

export default memo(Section);
