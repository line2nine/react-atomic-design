import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface PProps {
    children?: React.ReactNode;
    className?: string;

    [prop: string]: any;

    bgColor?: string;
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledP = styled.p<PProps>`
  color: #4598cd;
`;

const P = ({
               children,
               className,
               bgColor
           }: PProps) => {
    const classCandidate = [className];
    const commonProps = {
        bgColor
    };
    return <StyledP {...commonProps} className={cn(classCandidate)}>
        {children}
    </StyledP>
};

export default memo(P);
