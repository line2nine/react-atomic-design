import React, {memo} from 'react';
import Div from "../../atoms/View/Div";
import A from "../../atoms/A/A";
import Span from "../../atoms/View/Span";


function ContainerContentTabMenu() {
    return (
        <Div className="content-tab-menu">
            <A href="#" className="active-tab">
                <Span className="tab-number-active">1</Span>
                <Span>서버 이미지 선택</Span>
            </A>
            <A href="#" className="hover-tab">
                <Span className="tab-number">2</Span>
                <Span>서버 설정</Span>
            </A>
            <A href="#" className="hover-tab">
                <Span className="tab-number">3</Span>
                <Span>인증키 설정</Span>
            </A>
            <A href="#" className="hover-tab">
                <Span className="tab-number">4</Span>
                <Span>네트워크 접근 설정</Span>
            </A>
            <A href="#" className="hover-tab">
                <Span className="tab-number">5</Span>
                <Span>최종 확인</Span>
            </A>
        </Div>
    )
}

export default memo(ContainerContentTabMenu)