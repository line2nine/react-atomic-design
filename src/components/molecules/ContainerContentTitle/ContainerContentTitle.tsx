import React, {useState, useEffect, memo} from 'react';
import Div from "../../atoms/View/Div";
import Span from "../../atoms/View/Span";
import A from "../../atoms/A/A";
import ContainerContentDetail from "../ContainerContentDetail/ContainerContentDetail";


function ContainerContentTitle() {
    return (
        <Div className="content">
            <Div className="content-title">
                <Span className="content-header">서버 생성</Span>
                <Span className="content-sub-title">새로운 VM 서버를 생성합니다.</Span>
            </Div>
            <br/>
            <ContainerContentDetail/>
        </Div>
    )
}

export default memo(ContainerContentTitle)