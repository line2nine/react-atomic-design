import React, { useState, useEffect, memo } from 'react';
import Div from "../../atoms/View/Div";
import LeftMenuList from "../LeftMenuList/LeftMenuList";
import LeftMenuListItem from "../LeftMenuListItem/LeftMenuListItem";


function LeftMenuContent() {
    return (
        <Div className="sidebar-content">
            <LeftMenuList/>
            <LeftMenuListItem/>
        </Div>
    )
}

export default memo(LeftMenuContent)