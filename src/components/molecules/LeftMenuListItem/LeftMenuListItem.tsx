import React, { useState, useEffect, memo } from 'react';
import Div from "../../atoms/View/Div";
import A from "../../atoms/A/A";
import Span from "../../atoms/View/Span";
import Image from "../../atoms/Image/Image";


function LeftMenuListItem() {
    return (
        <Div className="sidebar-item">
            <A className="sidebar-link" href="#">
                <i className="fas fa-server"></i>
                &nbsp;
                <Span>Server</Span>
                <Span className="text-right">&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;-</Span>
            </A>
            <ul className="sidebar-item sidebar-menu">
                <li className="active">
                    <A className="sidebar-link" href="#">
                        <i className="far fa-circle"></i>
                        &nbsp;
                        <Span>Server</Span>
                    </A>
                </li>
                <li>
                    <A className="sidebar-link" href="#">
                        <i className="far fa-circle"></i>
                        &nbsp;
                        <Span>Bare Metal Server</Span>
                    </A>
                </li>
                <li>
                    <A className="sidebar-link" href="#">
                        <i className="far fa-circle"></i>
                        &nbsp;
                        <Span>Server Image</Span>
                    </A>
                </li>
                <li>
                    <A className="sidebar-link" href="#">
                        <i className="far fa-circle"></i>
                        &nbsp;
                        <Span>Server Image Builder</Span>
                    </A>
                </li>
                <li>
                    <A className="sidebar-link" href="#">
                        <i className="far fa-circle"></i>
                        &nbsp;
                        <Span>Storage</Span>
                    </A>
                </li>
                <li>
                    <A className="sidebar-link" href="#">
                        <i className="far fa-circle"></i>
                        &nbsp;
                        <Span>Snapshot</Span>
                    </A>
                </li>
                <li>
                    <A className="sidebar-link" href="#">
                        <i className="far fa-circle"></i>
                        &nbsp;
                        <Span>Public IP</Span>
                    </A>
                </li>
                <li>
                    <A className="sidebar-link" href="#">
                        <i className="far fa-circle"></i>
                        &nbsp;
                        <Span>Init Script</Span>
                    </A>
                </li>
                <li>
                    <A className="sidebar-link" href="#">
                        <i className="far fa-circle"></i>
                        &nbsp;
                        <Span>Private Subnet</Span>
                    </A>
                </li>
                <li>
                    <A className="sidebar-link" href="#">
                        <i className="far fa-circle"></i>
                        &nbsp;
                        <Span>Network Interface</Span>
                    </A>
                </li>
                <li>
                    <A className="sidebar-link" href="#">
                        <i className="far fa-circle"></i>
                        &nbsp;
                        <Span>ACG</Span>
                    </A>
                </li>
            </ul>
        </Div>
    )
}

export default memo(LeftMenuListItem)