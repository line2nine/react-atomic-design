import React, {useState, useEffect, memo} from 'react';
import Section from "../../atoms/View/Section";
import Table from "../../atoms/Table/Table";
import A from "../../atoms/A/A";
import Image from "../../atoms/Image/Image";
import TH from "../../atoms/Table/TH";
import TR from "../../atoms/Table/TR";
import TD from "../../atoms/Table/TD";


function ContainerContentDescriptionTwo() {
    return (
        <Section className="section-content-description-2">
            <Table className="table-2">
                <TR>
                    <TH className="left-text">서버 이미지 이름</TH>
                    <TH>설명</TH>
                    <TH>

                    </TH>
                </TR>
                <tbody>
                <TR>
                    <TD>
                        <Image className="icon-centos" src="resources/assets/images/icon/centos.png" alt=""/>
                            centos-7.3-64
                    </TD>
                    <TD>
                        CentOS 7.3 (64-bit)(커널 업데이트는 지원하지 않으며, 업데이트 진행시 VM 사용이 불가능하며, 복구를 지원하지 않습니다.)
                    </TD>
                    <TD><A href="#" className="btn">-| 다음</A></TD>
                </TR>
                <TR>
                    <TD>
                        <Image className="icon-centos" src="resources/assets/images/icon/centos.png" alt=""/>
                            centos-7.2-64
                    </TD>
                    <TD>
                        CentOS 7.2 (64-bit)(커널 업데이트는 지원하지 않으며, 업데이트 진행시 VM 사용이 불가능하며, 복구를 지원하지 않습니다.)
                    </TD>
                    <TD><A href="#" className="btn">-| 다음</A>
                    </TD>
                </TR>
                <TR>
                    <TD>
                        <Image className="icon-centos" src="resources/assets/images/icon/centos.png" alt=""/>
                            centos-6.6-64
                    </TD>
                    <TD>
                        CentOS 6.6 (64-bit)(커널 업데이트는 지원하지 않으며, 업데이트 진행시 VM 사용이 불가능하며, 복구를 지원하지 않습니다.)
                    </TD>
                    <TD><A href="#" className="btn">-| 다음</A></TD>
                </TR>
                <TR>
                    <TD>
                        <Image className="icon-centos" src="resources/assets/images/icon/centos.png" alt=""/>
                            centos-6.3-64
                    </TD>
                    <TD>
                        CentOS 6.3 (64-bit)(커널 업데이트는 지원하지 않으며, 업데이트 진행시 VM 사용이 불가능하며, 복구를 지원하지 않습니다.)
                    </TD>
                    <TD><A href="#" className="btn">-| 다음</A></TD>
                </TR>
                <TR>
                    <TD>
                        <Image className="icon-centos" src="resources/assets/images/icon/centos.png" alt=""/>
                            centos-6.3-32
                    </TD>
                    <TD>
                        CentOS 6.3 (32-bit)(커널 업데이트는 지원하지 않으며, 업데이트 진행시 VM 사용이 불가능하며, 복구를 지원하지 않습니다.)
                    </TD>
                    <TD><A href="#" className="btn">-| 다음</A></TD>
                </TR>
                <TR>
                    <TD>
                        <Image className="icon-centos" src="resources/assets/images/icon/ubuntu.png" alt=""/>
                            ubuntu-16.04-64-server
                    </TD>
                    <TD>
                        Ubuntu Server 16.04 (64-bit)(※ Ubuntu 커널 업데이트는 지원하지 않으며, 업데이트 진행시 VM 사용이 불가능하며, 복구를 지원하지
                        않습니다.)
                    </TD>
                    <TD><A href="#" className="btn">-| 다음</A></TD>
                </TR>
                </tbody>
            </Table>
        </Section>
    )
}

export default memo(ContainerContentDescriptionTwo)