import React, {useState, useEffect, memo} from 'react';
import Div from "../../atoms/View/Div";
import ContainerContentTitle from "../ContainerContentTitle/ContainerContentTitle";
import ContainerContentDetail from "../ContainerContentDetail/ContainerContentDetail";


function ContainerContent() {
    return (
        <Div className="container">
            <ContainerContentTitle/>
            <hr/>
        </Div>
    )
}

export default memo(ContainerContent)