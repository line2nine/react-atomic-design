import React, {useState, useEffect, memo} from 'react';
import Section from "../../atoms/View/Section";
import Div from "../../atoms/View/Div";
import ContainerContentTabMenu from "../ContainerContentTabMenu/ContainerContentTabMenu";
import ContainerContentDescriptionOne from "../ContainerContentDescriptionOne/ContainerContentDescriptionOne";
import ContainerContentDescriptionTwo from "../ContainerContentDescriptionTwo/ContainerContentDescriptionTwo";


function ContainerContentDetail() {
    return (
        <Section className="content-detail">
            <ContainerContentTabMenu/>
            <hr/>
            <br/>
            <ContainerContentDescriptionOne/>
            <ContainerContentDescriptionTwo/>
        </Section>
    )
}

export default memo(ContainerContentDetail)