import React, { useState, useEffect, memo } from 'react';
import Div from "../../atoms/View/Div";
import A from "../../atoms/A/A";
import Span from "../../atoms/View/Span";
import Image from "../../atoms/Image/Image";


function LeftMenuList() {
    return (
        <Div className="sidebar-head">
            <A className="sidebar-title" href="#">
                <Span>KL_NET</Span>
                <Span className="console">console</Span>
            </A>

            <Div className="sidebar-item">
                <A className="sidebar-link" href="#">
                    <Image className="sidebar-icon" src="resources/assets/images/icon/south-korea-flag.jpg" alt="#"/>
                    &nbsp;
                    <Span>Region 한국/한국어</Span>
                    <Span className="text-right"><i className="fas fa-angle-down"></i></Span>
                </A>
            </Div>

            <Div className="sidebar-item sidebar-group">
                <A className="sidebar-link" href="#">
                    <i className="fas fa-info-circle"></i>
                    &nbsp;
                    <Span>Dashboard</Span>
                </A>
            </Div>

            <Div class="sidebar-item sidebar-group">
                <A className="sidebar-link" href="#">
                    <i className="fas fa-list"></i>
                    &nbsp;
                    <Span>Product & Services</Span>
                    <Span className="text-right">+</Span>
                </A>
            </Div>

            <Div className="sidebar-item sidebar-group sidebar-bookmark">
                <A className="sidebar-link" href="#">
                    <Span>bookmarks <Span className="bookmark-number">0</Span></Span>
                    <Span className="text-right">EDIT <Span className="text-right mgl-20">-</Span></Span>
                </A>
            </Div>

            <Div className="sidebar-item sidebar-group sidebar-recently-viewed">
                <A className="sidebar-link" href="#">
                    <Span>recently Viewed</Span>
                </A>
            </Div>
        </Div>
    )
}

export default memo(LeftMenuList)