import React, { useState, useEffect, memo } from 'react';
import LeftMenuContent from "../LeftMenuContent/LeftMenuContent";
import Nav from "../../atoms/Nav/Nav";


function Sidebar() {
    return (
        <Nav className="sidebar">
            <LeftMenuContent/>
            <hr/>
        </Nav>
    )
}

export default memo(Sidebar)