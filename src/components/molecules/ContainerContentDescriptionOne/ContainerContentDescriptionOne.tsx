import React, {memo} from 'react';
import Section from "../../atoms/View/Section";
import Div from "../../atoms/View/Div";
import P from "../../atoms/View/P";
import Table from "../../atoms/Table/Table";
import Label from "../../atoms/Label/Label";


function ContainerContentDescriptionOne() {
    return (
        <Section className="section-content-description-1">
            <Div className="content-description">
                <h2 className="description-title">
                    CentOS, Ubuntu, Windows 및 DBMS 서버 이미지를 제공합니다. 이미지 및 부팅 디스크 크기를 선택하세요.
                </h2>
                <P>
                    - 현재 Windows 에 대해서만 부팅 디스크로 100GB 선택이 가능하며, VDS를 사용하려면 100GB를 선택하세요.
                </P>
                <P>
                    - GPU Server는 CentOS 7.3, Ubuntu 16.04, Tensorflow 등의 서버 이미지에서만 생성 가능합니다.
                </P>
            </Div>
            <Div>
                <Table className="table-1">
                    <tr>
                        <th>부팅 디스크 크기</th>
                        <td>
                            <input type="radio" id="radio-1" name="radio-1" value="1" checked={true}/>
                            <Label htmlFor="radio-1" className="mgr-50">50GB</Label>
                            <input type="radio" id="radio-1-1" name="radio-1" value="2"/>
                            <Label htmlFor="radio-1-1" className="normal">100GB (MSSQL은 부팅디스크 크기가 100GB 인 모델만 있습니다)</Label>
                        </td>
                    </tr>
                    <tr>
                        <th>이미지타입</th>
                        <td>
                            <input type="radio" id="radio-2" name="radio-2" value="1" checked={true}/>
                            <Label htmlFor="radio-2" className="mgr-66">OS</Label>
                            <input type="radio" id="radio-2-2" name="radio-2" value="2"/>
                            <Label htmlFor="radio-2-2" className="mgr-66">Application</Label>
                            <input type="radio" id="radio-2-3" name="radio-2" value="3"/>
                            <Label htmlFor="radio-2-3">DBMS</Label>
                        </td>
                    </tr>
                    <tr>
                        <th>OS 이미지타입</th>
                        <td>
                            <input type="radio" id="radio-3" name="radio-3" value="1" checked={true}/>
                            <Label htmlFor="radio-3" className="mgr-68">All</Label>
                            <input type="radio" id="radio-3-2" name="radio-3" value="2"/>
                            <Label htmlFor="radio-3-2" className="mgr-92">CentOS</Label>
                            <input type="radio" id="radio-3-3" name="radio-3" value="3"/>
                            <Label htmlFor="radio-3-3" className="mgr-70">Ubuntu</Label>
                            <input type="radio" id="radio-3-4" name="radio-3" value="4"/>
                            <Label htmlFor="radio-3-4">Windows</Label>
                        </td>
                    </tr>
                    <tr>
                        <th>서버 타입</th>
                        <td>
                            <input type="radio" id="radio-4" name="radio-4" value="1" checked={true}/>
                            <Label htmlFor="radio-4" className="mgr-48">Micro</Label>
                            <input type="radio" id="radio-4-2" name="radio-4" value="2"/>
                            <Label htmlFor="radio-4-2" className="mgr-82">Compact</Label>
                            <input type="radio" id="radio-4-3" name="radio-4" value="3"/>
                            <Label htmlFor="radio-4-3" className="mgr-59">Standard</Label>
                            <input type="radio" id="radio-4-4" name="radio-4" value="4"/>
                            <Label htmlFor="radio-4-4" className="mgr-50">High-Memory</Label>
                            <input type="radio" id="radio-4-5" name="radio-4" value="5"/>
                            <Label htmlFor="radio-4-5" className="mgr-50">CPU Intensive</Label>
                            <input type="radio" id="radio-4-6" name="radio-4" value="6"/>
                            <Label htmlFor="radio-4-6" className="mgr-50">GPU</Label>
                            <input type="radio" id="radio-4-7" name="radio-4" value="7"/>
                            <Label htmlFor="radio-4-7">VDS</Label>
                        </td>
                    </tr>
                </Table>
            </Div>
        </Section>
    )
}

export default memo(ContainerContentDescriptionOne)