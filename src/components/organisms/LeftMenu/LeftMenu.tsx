import React, {memo} from "react";
import Sidebar from "../../molecules/Sidebar/Sidebar";

function LeftMenu() {
    return (
        <Sidebar/>
    )
}

export default memo(LeftMenu)