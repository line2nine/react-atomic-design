import React, {memo} from "react";
import HeaderDashboard from "../organisms/HeaderDashboard/HeaderDashboard";
import LeftMenu from "../organisms/LeftMenu/LeftMenu";
import Div from "../atoms/View/Div";
import Container from "../organisms/Container/Container";

function Dashboard() {
    return (
        <Div className="wrapper">
            <HeaderDashboard/>
            <LeftMenu/>
            <Container/>
        </Div>
    )
}

export default memo(Dashboard)